# ADF Calendar step-by-step implementation

http://www.gebs.ro/blog/oracle/oracle-adf-calendar-step-by-step-implementation/

    CREATE TABLE CALENDAR
    (
      ID VARCHAR2(20 CHAR) NOT NULL
    , PROVIDER VARCHAR2(100 CHAR) NOT NULL
    , START_DATE DATE NOT NULL
    , END_DATE DATE NOT NULL
    , DESCRIPTION VARCHAR2(200 CHAR) NOT NULL
    , CONSTRAINT CALENDAR_PK PRIMARY KEY
      (
        ID
      )
      ENABLE
    )


This app works not so good on Jdev 12.2.
But we can improve it.

Please send pull request to improve.
